<?php

namespace App\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use UserApiApp\Domain;

/**
 * Class PushDownloadNotificationJob
 * @package App\Jobs
 */
class PushDownloadNotificationJob extends Job implements ShouldQueue, PushDownloadNotification
{
    use InteractsWithQueue, SerializesModels;

   private $userDownloads

    /**
     * PushDownloadNotificationJob constructor.
     *
     * @param MaropostUserRepository $maropost_repository
     * @param GigyaUserRepository    $gigya_repository
     */
    public function __construct(Domain\Api\UserDownloads $userDownloads)
    {
        $this->userDownloads = $userDownloads;
    }

    /**
     * Executes the job.
     */
    public function handle()
    {
        $this->userDownloads->post($payloadData);
    }
}
