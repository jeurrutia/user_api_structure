<?php

namespace App\Http\Controllers\User;

use App\Entities\DownloadEntity;
use App\Http\Requests\UserDownloadNotificationRequest;
use App\Jobs\Contracts\PushDownloadNotification;
use App\Jobs\PushDownloadNotificationJob;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

/**
 * Class DownloadsController
 *
 * @package App\Http\Controllers\User
 */
class DownloadsController extends Controller
{
    /**
     * @var PushDownloadNotificationJob
     */
    private $push_download_notification_job;

    /**
     * DownloadsController constructor.
     *
     * @param PushDownloadNotification $push_download_notification_job
     */
    public function __construct(PushDownloadNotification $push_download_notification_job)
    {
        $this->push_download_notification_job = $push_download_notification_job;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\UserDownloadNotificationRequest $request
     * @param \App\Entities\DownloadEntity                       $download_entity
     * @param                                                    $user_id
     *
     * @return Response
     */
    public function store(UserDownloadNotificationRequest $request, DownloadEntity $download_entity, $user_id)
    {
        $download_entity->user_id        = $user_id;
        $download_entity->email          = $request->input('email');
        $download_entity->program_id     = $request->input('program_id');
        $download_entity->platform_id    = $request->input('platform_id');
        $download_entity->category_id    = $request->input('category_id');
        $download_entity->subcategory_id = $request->input('subcategory_id');
        $download_entity->os             = $request->input('os');
        $download_entity->downloaded_at  = $request->input('downloaded_at');
        $download_entity->browser_name   = $request->input('browser_name');

        $this->dispatch($this->push_download_notification_job->setMessage($download_entity));

        return response(json_encode([ 'message' => 'Accepted.' ], true), 202);
    }
}
