<?php
/**
 * Created by PhpStorm.
 * User: javier.eurrutia
 * Date: 27/10/16
 * Time: 12:17
 */

namespace UserApiApp\Infrastructure\Builders;

use UserApiApp\Infrastructure\Entities\UserApi\User\Download;
use UserApiApp\Infrastructure\Builders\BuilderInterface;

class UserDownloads implements BuilderInterface
{
    public function buildFromPayloadData(array $payload)
    {
        $downloadEntity = new Download();
        $downloadEntity->user_id        = $user_id;
        $downloadEntity->email          = $request->input('email');
        $downloadEntity->program_id     = $request->input('program_id');
        $downloadEntity->platform_id    = $request->input('platform_id');
        $downloadEntity->category_id    = $request->input('category_id');
        $downloadEntity->subcategory_id = $request->input('subcategory_id');
        $downloadEntity->os             = $request->input('os');
        $downloadEntity->downloaded_at  = $request->input('downloaded_at');
        $downloadEntity->browser_name   = $request->input('browser_name');
    }
}