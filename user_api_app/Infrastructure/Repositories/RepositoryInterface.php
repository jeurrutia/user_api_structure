<?php
/**
 * Created by PhpStorm.
 * User: javier.eurrutia
 * Date: 27/10/16
 * Time: 11:54
 */

namespace UserApiApp\Infrastructure\Repositories;

interface RepositoryInterface
{
    public function create(array $payloadData);

    public function read();

    public function update();

    public function delete();
}