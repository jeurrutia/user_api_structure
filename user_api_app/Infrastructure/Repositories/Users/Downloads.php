<?php
/**
 * Created by PhpStorm.
 * User: javier.eurrutia
 * Date: 27/10/16
 * Time: 11:35
 */

namespace UserApiApp\Infrastructure\Repositories\Users;

use UserApiApp\Infrastructure\Datasources\DatasourceInterface;
use UserApiApp\Infrastructure\Repositories\RepositoryInterface;
use UserApiApp\Infrastructure\Builders\BuilderInterface;

class Downloads implements RepositoryInterface
{
    private $maropostDatasource;
    
    private $userDownloadsBuilder;

    public function __construct(DatasourceInterface $maropostDatasource, BuilderInterface $userDownloadsBuilder)
    {
        $this->maropostDatasource   = $maropostDatasource;
        $this->userDownloadsBuilder = $userDownloadsBuilder;
    }

    public function create(array $payloadData)
    {
        $downloadEntity = $this->userDownloadsBuilder->buildFromPayloadData($payloadData);
        $this->maropostDatasource->save($downloadEntity);
    }

    public function read()
    {

    }

    public function update()
    {

    }

    public function delete()
    {

    }
}