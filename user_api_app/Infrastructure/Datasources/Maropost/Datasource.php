<?php
/**
 * Created by PhpStorm.
 * User: javier.eurrutia
 * Date: 27/10/16
 * Time: 11:59
 */
namespace UserApiApp\Infrastructure\Datasources\Maropost;

use UserApiApp\Infrastructure\Datasources\DatasourceInterface;
use UserApiApp\Infrastructure\Entities\EntityInterface;
use UserApiApp\Infrastructure\Entities\UserInterface;

class Datasource implements DatasourceInterface
{
    public function find() : array
    {
        
    }

    public function findById($uid) : UserInterface
    {
        
    }

    public function save(EntityInterface $downloadsEntity)
    {
        
    }

    public function remove($uid)
    {
        
    }
}