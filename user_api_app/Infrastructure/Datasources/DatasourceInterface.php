<?php
/**
 * Created by PhpStorm.
 * User: javier.eurrutia
 * Date: 27/10/16
 * Time: 11:41
 */

namespace UserApiApp\Infrastructure\Datasources;

use UserApiApp\Infrastructure\Entities\EntityInterface;

interface DatasourceInterface
{
    public function find() : array;

    public function findById($uid) : EntityInterface;

    public function save(EntityInterface $userEntity);

    public function remove($uid);
}