<?php
/**
 * Created by PhpStorm.
 * User: javier.eurrutia
 * Date: 27/10/16
 * Time: 12:20
 */
namespace UserApiApp\Infrastructure\Entities\UserApi\User;

class Download
{
    /**
     * User id.
     *
     * @var int
     */
    public $user_id;

    /**
     * User e-mail.
     *
     * @var string
     */
    public $email;

    /**
     * Id of the program.
     *
     * @var int
     */
    public $program_id;

    /**
     * Id of the platform.
     *
     * @var int
     */
    public $platform_id;

    /**
     * Id of the category.
     *
     * @var int
     */
    public $category_id;

    /**
     * Id of the sub-category.
     *
     * @var int
     */
    public $subcategory_id;

    /**
     * User operating system.
     *
     * @var string
     */
    public $os;

    /**
     * DateTime when the download has been done.
     *
     * @var string
     */
    public $downloaded_at;

    /**
     * User browser name.
     *
     * @var string
     */
    public $browser_name;

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return int
     */
    public function getProgramId()
    {
        return $this->program_id;
    }

    /**
     * @param int $program_id
     */
    public function setProgramId($program_id)
    {
        $this->program_id = $program_id;
    }

    /**
     * @return int
     */
    public function getPlatformId()
    {
        return $this->platform_id;
    }

    /**
     * @param int $platform_id
     */
    public function setPlatformId($platform_id)
    {
        $this->platform_id = $platform_id;
    }

    /**
     * @return int
     */
    public function getCategoryId()
    {
        return $this->category_id;
    }

    /**
     * @param int $category_id
     */
    public function setCategoryId($category_id)
    {
        $this->category_id = $category_id;
    }

    /**
     * @return int
     */
    public function getSubcategoryId()
    {
        return $this->subcategory_id;
    }

    /**
     * @param int $subcategory_id
     */
    public function setSubcategoryId($subcategory_id)
    {
        $this->subcategory_id = $subcategory_id;
    }

    /**
     * @return string
     */
    public function getOs()
    {
        return $this->os;
    }

    /**
     * @param string $os
     */
    public function setOs($os)
    {
        $this->os = $os;
    }

    /**
     * @return string
     */
    public function getDownloadedAt()
    {
        return $this->downloaded_at;
    }

    /**
     * @param string $downloaded_at
     */
    public function setDownloadedAt($downloaded_at)
    {
        $this->downloaded_at = $downloaded_at;
    }

    /**
     * @return string
     */
    public function getBrowserName()
    {
        return $this->browser_name;
    }

    /**
     * @param string $browser_name
     */
    public function setBrowserName($browser_name)
    {
        $this->browser_name = $browser_name;
    }
}