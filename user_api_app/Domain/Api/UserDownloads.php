<?php
/**
 * Created by PhpStorm.
 * User: javier.eurrutia
 * Date: 27/10/16
 * Time: 12:07
 */

namespace UserApiApp\Domain\Api;

use UserApiApp\Infrastructure\Repositories\RepositoryInterface;

class UserDownloads
{
    private $userDownloadsRepository;
    
    public function __construct(RepositoryInterface $userDownloadsRepository)
    {
        $this->userDownloadsRepository = $userDownloadsRepository;
    }

    public function post(array $payloadData)
    {
        // Do business rules validations...
        // Call to main process -> repository
        $this->userDownloadsRepository->create($payloadData);
    }
    
    public function get(array $parameters)
    {
        // ...
    }
    
    public function put(array $payloadData)
    {
        // ...
    }
    
    public function patch(array $payloadData)
    {
        // ...
    }
    
    public function delete(array $parameters)
    {
        // ...
    }
}